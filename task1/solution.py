from functools import wraps
from typing import List


class SimpleResponse:

    def __init__(self):
        self.resp: List[int] = [1, 2, 3]

    def response(self) -> List[int]:
        # super business logic...
        return self.resp


class DecoratedResponse(SimpleResponse):

    def __init__(self):
        super().__init__()

    @staticmethod
    def decorator(fn):
        @wraps
        def wrapper(*args, **kwargs):
            res = fn(*args, **kwargs)
            return [el ** 2 for el in res]
        return wrapper

    @decorator
    def response(self) -> List[int]:
        return super().response()


if __name__ == '__main__':
    d = DecoratedResponse()
    a = d.response()
    print(f"source: {d.resp} \nresult: {a}")
