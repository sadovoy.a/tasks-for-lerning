# С одной стороны хорошо, что проверяешь типы данных. С другой - в цикле проверять isinstance - убийство производительности.
# Предложение - более корректно типизировать входной/выходной тип
# например number: def get_square_number_list(number: List[int]) -> List[int]:
# принт делать не надо. Выкидывай исключение
def get_square_number_list(number: list) -> list:
    if isinstance(number, list):
        return [item ** 2 for item in number if isinstance(item, int)]
    else:
        print("данные не являются списком")


# и зачем тут неиспользуемый method: classmethod? это выражение вообще не имеет никакого смысла
def my_decor(method: classmethod):
    def new_method(*args) -> list:  # тогда прокидывай и *kwargs
        self = args[0]  # так вообще делать не надо. поломать self очень легко. при отладке в реальном коде будешь долго искать источник
        return get_square_number_list(self.resp)  # self.resp - в жизни не догадаешься что в resp приходит.
    return new_method


class SimpleResponse:

    def __init__(self):
        self.resp = [1, 2, 3]

    def response(self) -> list:
        pass  # метод в задании возвращал список из свойства класса


class DecoratedResponse(SimpleResponse):
    @my_decor
    def response(self) -> list:
        return self.resp


a = DecoratedResponse()
print(a.response())

# так же нет requirements.txt. нет описания версии питона. надо показать простой тулинг для базового проекта
