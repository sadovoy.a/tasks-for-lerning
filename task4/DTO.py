from typing import NamedTuple

from pydantic import BaseModel, ConfigDict

from task4.model import Result


class BaseDirFolder(BaseModel):
    base_folder: str


class FilesName(BaseModel):
    file_name: str


class DTOTransferModel(BaseModel):
    id: int
    source: str
    destination: str
    result: Result
    message: str


class DTOTransferFile(BaseModel):
    model_config = ConfigDict(from_attributes=True)
    id: int
    source: str
    destination: str
    result: Result
    message: str


class ListTransfer(NamedTuple):
    source: str
    destination: str
    result: str
    message: str

