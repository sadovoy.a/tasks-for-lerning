from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine, select
from sqlalchemy.engine import Result

from task4.DTO import ListTransfer
from task4.config import DB_HOST, DB_PORT, DB_USER, DB_NAME, DB_PASS
from task4.model import TransferModel


url_db = f"postgresql+psycopg://{DB_USER}:{DB_PASS}@{DB_HOST}:{DB_PORT}/{DB_NAME}"

engine = create_engine(url_db)


session_factory = sessionmaker(engine)


def db_add(data_move: list[ListTransfer]):
    for item in data_move:
        stmt = TransferModel(source=item.source,
                             destination=item.destination,
                             result=item.result,
                             message=item.message)
        with session_factory() as session:
            session.add(stmt)
            session.commit()


class GetDataFromBase:

    @staticmethod
    def get_db_all_files() -> list[TransferModel]:
        with session_factory() as session:
            smtp = select(TransferModel)
            result: Result = session.execute(smtp)
            return list(result.scalars().all())

    @staticmethod
    def get_db_from_id(id_file: int) -> TransferModel | None:
        with session_factory() as session:
            return session.get(TransferModel, id_file)

    @staticmethod
    def get_filename(text_search: str) -> list[TransferModel]:
        with session_factory() as session:
            smtp = select(TransferModel
                          ).where(TransferModel.source.ilike(f"%{text_search}%"))
            result: Result = session.execute(smtp)
            return list(result.scalars().all())









