from sqlalchemy.orm import Mapped, DeclarativeBase, mapped_column
import enum


class Base(DeclarativeBase):
    pass


class Result(enum.Enum):
    error = 0
    success = 1


class TransferModel(Base):
    __tablename__: str = "transfer_table"

    id: Mapped[int] = mapped_column(primary_key=True)
    source: Mapped[str]
    destination: Mapped[str]
    result: Mapped[Result] = mapped_column(default='success')
    message: Mapped[str]








