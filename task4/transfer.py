from dataclasses import dataclass
from collections import namedtuple
from typing import NamedTuple

from pathlib import Path
from typing import List, Dict, Type

from loguru import logger
import shutil

from task4.DTO import DTOTransferModel, ListTransfer
from task4.database import db_add


@dataclass
class PathFiles:
    source: str
    new_path: str


logger.add("loger.json", format="{level}{message}", level="INFO", serialize=True)
extensions: list = [".jpeg", ".png", ".jpg"]


class TransferImages:
    def __init__(self, base_dir_folder: str):
        self.base_dir_folder = base_dir_folder

    @property
    def get_path_images(self) -> list[Path]:
        return [files for files in Path(self.base_dir_folder).rglob('*') if files.suffix in extensions]

    @property
    def paths_from_transfer(self) -> list[PathFiles]:
        return [PathFiles(str(file), str(file.parent.parent)) for file in self.get_path_images
                if file.parent.stem == "images"]

    @staticmethod
    def move_files(paths: list[PathFiles]) -> list[ListTransfer]:
        list_transfers = []
        for folder in paths:
            try:
                shutil.move(folder.source, folder.new_path)
                logger.info(f"Файл {folder.source},перенесен в паку {folder.new_path}")

                list_transfers.append(ListTransfer(source=folder.source,
                                                   destination=folder.new_path,
                                                   result="success",
                                                   message="ok"
                                                   ))
            except OSError as ex:
                logger.exception(f"Файл {folder.source}, не был перенесен в паку {folder.new_path} : {ex}")
                list_transfers.append(ListTransfer(source=folder.source,
                                                   destination=folder.new_path,
                                                   result="error",
                                                   message=ex.strerror
                                                   ))
        return list_transfers

    def get_delete_folders(self) -> list[str]:
        return [files for files in map(str, Path(self.base_dir_folder).rglob('images'))]

    @staticmethod
    def move_one_file(path_file: PathFiles):
        try:
            shutil.move(path_file.source, path_file.new_path)
        except OSError as ex:
            logger.exception(f"Файл {path_file.source}, не был перенесен в паку {path_file.new_path} : {ex}")

    def map_method_move_files(self, paths: list[PathFiles]):
        res = map(self.move_one_file, paths)
        a = list(res)

    @staticmethod
    def delete_folders(folders: list[str]) -> None:
        for folder in folders:
            try:
                shutil.rmtree(folder)
                logger.info(f"Удаление {folder}, успешно")
            except OSError as ex:
                logger.exception(f"Папка {folder}, не была удалена: {ex} ")

    def transfer_service(self):
        #self.map_method_move_files(self.paths_from_transfer)
        list_from_base = self.move_files(self.paths_from_transfer)
        db_add(list_from_base)
        self.delete_folders(self.get_delete_folders())


def cli(base_dir_folder: str):
    """
    Перенос изображений в указанном каталоге
    :param base_dir_folder: Исходная папка для обработки
    :return:
    """
    service = TransferImages(base_dir_folder)
    service.transfer_service()


if __name__ == "__main__":
    cli("/media/hortus/Новый том/models")
