from fastapi import FastAPI
from fastapi.responses import JSONResponse

from sqlalchemy import select

from task4.DTO import BaseDirFolder, ListTransfer, DTOTransferModel, DTOTransferFile, FilesName
from task4.database import engine
from task4.model import TransferModel
from task4.database import GetDataFromBase
from task4.transfer import cli

app = FastAPI(description="transfer folders")


@app.get("/")
def index():
    """

    :return:
    """
    return {
            "Name app": "Transfer images",
            "path": "http://127.0.0.1:8000/transfer",
            "type": "Post",
            "body": {"base_dir_folder": "path to base"}
           }


@app.post("/transfer")
def transfer_files(base_dir_folder: BaseDirFolder):
    """
    :param base_dir_folder:
    :return:
    """
    cli(base_dir_folder.base_folder)
    return {"status": "ok", "message": "success"}


@app.get("/get_move_all_files", response_model=list[DTOTransferFile])
def get_move_all_files():
    return GetDataFromBase.get_db_all_files()


@app.get("/get_move_files/{id_move}", response_model=DTOTransferFile)
def get_move_files(id_move: int):
    return GetDataFromBase.get_db_from_id(id_move)


@app.post("/get_files", response_model=list[DTOTransferFile])
def get_files(file_name: FilesName):
    return GetDataFromBase.get_filename(file_name.file_name)


